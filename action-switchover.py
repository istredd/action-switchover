#!/usr/bin/env python

import os, argparse, sys, urllib2, re, subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--url", dest="url", required=True,
                help="url address to be checked")
parser.add_argument("--script", dest="script", required=True,
                help="external script name to be run")
parser.add_argument("--params", dest="params",
                help="additional parameters to script")
parser.add_argument("--attempts", dest="attempts", default=3, type=int,
                help="number of attempts to try [default 3]")
parser.add_argument("--timeout", dest="timeout", default=2, type=int,
                help="server timeout [default 2 seconds]")
args = parser.parse_args()

def sudocheck():
    if os.geteuid() != 0:
        sys.exit("You need root permission to contiune")
        return

def page():
        try:
            if "up" in urllib2.urlopen(args.url, timeout = args.timeout).read().lower():
                status = "up\n"
            else:
                status = "down\n"
        except:
            status = "error\n"
        print status
        return status

def writestatus ():
    statuslist = [page()] + open('.statusfile', 'a+').readlines()
    open('.statusfile', 'w').writelines(statuslist[:args.attempts])
    if "down" in open('.statusfile', 'r').readline():
        downcount = -1
    else:
        downcount = len(re.findall("error|down", open('.statusfile').read()))
    return downcount

def scripttrigger ():
    errors = writestatus()
    if (errors == args.attempts) or (errors == -1):
        if os.path.exists(args.script):
            os.system("bash " + args.script + " " + args.params)
        else:
            print "File", args.script, "doesn't exists. Please check path and try again"

sudocheck()
scripttrigger()
