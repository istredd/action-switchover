#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
  echo "This script requires additional parameters to be provided."
  echo "Syntax: action-switchover.sh url attemtps script"
  echo "  - url:        url address to be checked"
  echo "  - attemtps:   maximum number of attempts to check url"
  echo "  - script:     external script to be run"
  echo "  - parameters: additional parameters forwarded to script (optional) "
  exit 1
fi

if [ "$(id -u)" != "0" ] ; then
	echo "This script requires to be run as root."
	exit 1
fi

mkdir -p ./.tmp

url="$1"
attempts="$2"
script="$3"
extraargs="$4 $5 $6"
httpcode="$(curl -s $url -w "%{http_code}" -o ./.tmp/.page)"
pagestatus="$(cat /.tmp/.page | grep -oi 'up\|down')"
statusfile="./.tmp/.status"

function removetmppage {
  rm -rf ./.tmp/.page
}

function runscript {
  if [ -a $script ] ; then
    bash $script $extraargs
  else
    echo "$script doesn't exists. Check path and try again"
  fi
}

if [ -z "$pagestatus" ] ; then
  pagestatus="error"
fi

echo "$pagestatus,$httpcode" >> $statusfile
if [ $(wc -l < $statusfile) -gt $attempts ] ; then
  sed -i '1d' $statusfile
fi

if [ $pagestatus == "down" ] ; then
  runscript
else
  errorscount="$(cat $statusfile  | cut -d',' -f1 | grep -cio 'down\|error')"
  if [ $errorscount == $attempts ] ; then
    runscript
  fi
fi

removetmppage
